import { Selector } from 'testcafe';

fixture `Google`
    .page `https://accounts.google.com`;

test('New Test', async t => {
    await t
        .maximizeWindow()
        .typeText(Selector('#identifierId'), 'knightgtt')
        .click(Selector('#identifierNext').find('.CwaK9'))
        .typeText(Selector('.whsOnd.zHQkBf[name="password"][data-initial-dir="ltr"]'), 'P@$$w0rd')
        .click(Selector('.RveJvd.snByac').nth(0))
        .hover(Selector('.gb_x.gb_Da.gb_f').find('span'))
        .click(Selector('.gb_x.gb_Da.gb_f').find('span'))
        .click(Selector('#gb_71'))
        .click(Selector('.UXurCe').nth(1).find('div').withText('Usu'))
        .click(Selector('.n3x5Fb').find('svg').find('path').nth(1))
        .click(Selector('.CwaK9').nth(3).find('span').withText('Tak'));
});